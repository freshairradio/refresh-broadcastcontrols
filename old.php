<?php


$prefix = "/home/techteam/";
$pid = $prefix."magic/liquidsoap.pid";

function checkLive() {
    global $pid;
    if (file_exists($pid)) {
        $f = fopen($pid, "rb");
        $process = fread($f, 128);
    } else
        $process = "";

    if ($process and file_exists("/proc/".$process))
        return True;
    else
        return False;
}

$live = checkLive();

$allowed = False;
if ( in_array($_SERVER['REMOTE_ADDR'], ["192.168.1.11","192.168.1.12","::1","127.0.0.1"]) ) {
    $allowed = True;

    switch ($_GET['live']) {
        case "start":
            exec($prefix."magic/start-live > /dev/null");
            header("Location: /");
            exit;
        case "stop":
            exec($prefix."magic/stop-live > /dev/null");
            header("Location: /");
            exit;
    }

    if ($_GET['song'] and $_GET['artist']) {
        $f = fopen($prefix."currentsong/override.txt",'w');
        $current = fwrite($f,$_GET['artist']." - ".$_GET['song']);
        fclose($f);
    }

}

$f = fopen($prefix."currentsong/currentsong.txt",'r');
$current = fgets($f);
fclose($f);

?><!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FreshAir.org.uk - Broadcast Server</title>
<link rel="stylesheet" href="/style.css" type="text/css" />
<meta http-equiv="refresh" content="30">
</head>
<body>
<div class="wrapper">
<div class="message">Studio 1 is currently</div>
<div class="status<?=($live?" live":" offline");?>"><?=($live?"Live":"Off-Air");?></div>
<?php if ($allowed): ?>
<div class="switch"><a href="/?live=<?=($live?"stop":"start");?>"><?=($live?"Stop":"Start");?> Live Broadcast</a></div>
<?php else: ?>
<div class="message" style="font-size: 10px;">You must be on PC1 or PC2 in Studio 1 to access the Broadcast Controls</div>
<?php endif; ?>
</div>
<?php if ($allowed and $live and False): ?>
<div class="wrapper">
<div class="message">Song Override</div>
<div class="song">Current Song: <strong><?=$current?></strong></div>
<form>Artist: <input type="text" name="artist" /> Song: <input type="text" name="song" /> <input type="submit" value="Override"></form>
</div>
<?php endif; ?>
<div class="wrapper" style="height: 188px;">
<img src="http://webcam.freshair.org.uk/1" alt="" style="width: 240px; padding: 4px;" />
<img src="http://webcam.freshair.org.uk/2" alt="" style="width: 240px; padding: 4px;"/>
</div>
</body>
</html>
