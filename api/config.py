ALLOWED = ["127.0.0.1", "192.168.1.11", "192.168.1.12", "::1", "129.215.245.85","129.215.245.82"]

UPLOAD_FOLDER = '/export/prerecordings/'
ALLOWED_EXTENSIONS = set(['mp3'])
OB_USERNAME = 'outside'
OB_PASSWORD = 'outside'

AUTOMATION_INFO = {
    'status': 'FreshSounds',
    'slug': None,
    'title': 'The best music from FreshAir',
    'link': None,
    'pic': None
}
