activate_virtualenv = '/var/www/refresh-broadcastcontrols/virtualenv/bin/activate_this.py'
execfile(activate_virtualenv, dict(__file__=activate_virtualenv))

import sys
sys.path.insert(0, '/var/www/refresh-broadcastcontrols/api/')
from api import app as application
