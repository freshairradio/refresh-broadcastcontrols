from liquid_io import liquid_io
from helpers import handle_metadata
from flask import Response


def is_live():
    if liquid_io('live', 'live.status') == "on":
        return True
    return False


def start_live():
    if is_live():
        return Response('Broadcast is already Live', 403, '')
    else:
        if liquid_io('live', 'live.start'):
            return "Broadcast Started"
        return Response('Broadcast failed to start', 500, '')


def stop_live():
    if not is_live():
        return Response('Broadcast is not Live', 403, '')
    else:
        if liquid_io('live', 'live.stop'):
            return "Broadcast Stopped"
    return Response('Broadcast failed to stop', 500, '')


def get_meta():
    try:
        return handle_metadata(liquid_io('radio', 'radio.metadata'))
    except Exception as e:
        return {"error": e.message}


def set_meta(meta_data):
    if not meta_data["artist"]:
        meta_data["artist"] = "FreshAir Radio"

    meta_string = ','.join(['%s="%s"' % (key, value) for (key, value) in meta_data.items()])
    meta_string = str(meta_string)

    try:
        liquid_io('radio', 'metadata.insert ' + meta_string)
    except Exception as e:
        return {"error": e.message}
    return meta_data
