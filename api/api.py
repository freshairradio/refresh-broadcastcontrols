from config import *
from logic import *

from functools import wraps
from datetime import datetime, timedelta
import time
import os
import json

from flask import Flask
from flask import request, Response
from werkzeug import secure_filename

import requests
import redis
import bleach
from mutagen.mp3 import MP3

app = Flask(__name__, static_folder='..', static_url_path='')
db = redis.StrictRedis(host='localhost', port=6379, db=0)


def check_auth(username, password):
    return username == OB_USERNAME and password == OB_PASSWORD


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def require_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if request.remote_addr in ALLOWED:
            return f(*args, **kwargs)
        auth = request.authorization
        if auth and check_auth(auth.username, auth.password):
            return f(*args, **kwargs)
        return Response('Not Authorized', 401, '')
    return decorated


def local_only(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if request.remote_addr in ALLOWED:
            return f(*args, **kwargs)
        return Response('Not Authorized', 401, '')
    return decorated


@app.route('/')
def version():
    return 'FreshAir Studio API v0.3 - Connecting from ' + request.remote_addr


@app.route('/auth/')
@require_auth
def auth():
    return 'Authenticated'


@app.route('/live/')
def checkLive():
    if is_live():
        return 'On-Air'
    else:
        return 'Off-Air'


@app.route('/broadcast_info/', methods=['GET', 'OPTION'])
def get_broadcast_info():
    # Default, declare automation
    info = AUTOMATION_INFO

    # If On Air, overwrite with broadcast info
    if is_live():
        info = db.hgetall('broadcast_info')
        # solve problem with way None value is stored in DB
        for key in info:
            if info[key] == 'None':
                info[key] = None

        # For Fresh Fringe
        info['status'] = 'On air'
        info['title'] = 'FreshFringe'
        info['pic'] = '/assets/fresh-fringe-round-e59c7961af9249bac4b29dac4df5eb67c2ee110103d60357a3ac04e4943d37c6.png'

    # If a prerecording is registered for now, overwrite with that
    now = int(datetime.now().strftime('%s'))
    three_hours = int(timedelta(hours=3).total_seconds())
    three_hours_ago = now - three_hours
    future = '+inf'

    prerecordings = db.zrangebyscore('prerecordings', three_hours_ago, future)
    for prerecording in prerecordings:
        prerecording = json.loads(prerecording)
        start_time = int(prerecording['time'])
        length = int(prerecording['length'])
        end_time = start_time + length

        playing_now = (start_time <= now) and (end_time > now)
        if (playing_now):
            info = {
                status:'Prerecording',
                slug: prerecording['slug'],
                title: prerecording['title'],
                link: prerecording['link'],
                pic: prerecording['pic']
                }
            break

    response = Response(json.dumps(info))
    response.headers['Access-Control-Allow-Origin'] = 'https://freshair.org.uk'

    return response


@app.route('/broadcast_info/', methods=['POST'])
@local_only
def set_broadcast_info():
    info = json.loads(request.data)
    show_title = info['title']

    db.delete('broadcast_info')
    db.hmset('broadcast_info', info)

    time_now = int(time.time())
    db.zadd('login_records', time_now, show_title)
    return ''


@app.route('/login_records/', methods=['GET'])
def get_login_records():
    start_time = request.args.get('start', default='0')
    end_time = request.args.get('end', default='+inf')
    records = db.zrangebyscore('login_records', start_time, end_time, withscores=True)

    return json.dumps(records)


@app.route('/messages/', methods=['GET'])
def get_last_messages():
    messages = db.lrange('messages', 0, 4)
    return json.dumps(messages)


@app.route('/messages/submit', methods=['POST'])
def submit_message():
    if request.form != "":
        message_received = request.form
        message = {
            "author": message_received["author"],
            "content": bleach.clean(message_received["content"], tags=[]),
            "time": message_received["time"],
            "date": message_received["date"]
            }
        message = json.dumps(message)
        db.lpush('messages', message)
    return ''


@app.route('/live/start/')
@local_only
def liveStart():
    return start_live()


@app.route('/live/stop/')
@local_only
def liveStop():
    return stop_live()


@app.route('/current_track/', methods=['GET'])
def getCurrent():
    return json.dumps(get_meta())


@app.route('/current_track/', methods=['POST'])
@require_auth
def setCurrent():
    meta_data = json.loads(request.data)
    return json.dumps(set_meta(meta_data))


@app.route('/current/mairlist/', methods=['GET', 'POST'])
@local_only
def setCurrentmAirList():
    artist = request.form.get('artist')
    title = request.form.get('title')
    album = request.form.get('album')
    track = request.form.get('track')
    meta_data = {"artist": artist, "title": title, "album": album, "track": track}
    return json.dumps(set_meta(meta_data))


@app.route('/prerecording/upload/', methods = ['POST'])
# @local_only
def upload_prerecording():
    if 'file' not in request.files:
        return Response('Upload Failed', 500, '')

    file = request.files['file']
    if file.filename == '':
        return Response('Upload Failed', 500, '')

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        return 'Upload successful'

    return Response('Upload Failed', 500, '')


@app.route('/prerecording/register/', methods = ['POST'])
# @local_only
def register_prerecording():
    filename = secure_filename(request.form['fileName'])
    path = os.path.join(UPLOAD_FOLDER, filename)
    audio = MP3(path)
    length = int(audio.info.length)
    show_slug = request.form['slug']
    show_title = request.form['title']
    show_link = request.form['link']
    show_pic = request.form['pic']
    time = int(request.form['broadcastTime'])

    r = requests.get('https://freshair.org.uk/api/shows/' + show_slug + '/check-broadcast-time/' + time + '-'
                     + str(time + length)).content
    if r != 'ok\n':
        return Response('Show not allocated to time or file too long', 500, '')

    prerecording = json.dumps({
        'slug': show_slug,
        'title': show_pic,
        'link': show_link,
        'pic': show_pic,
        'path': path,
        'time': time,
        'length': length
        })
    db.zadd('prerecordings', time, prerecording)
    return 'Ok'


@app.route('/prerecording/schedule/')
def prerecordings_schedule():
    now = int(datetime.now().strftime('%s'))
    lower_bound = int(now - timedelta(days=2).total_seconds())
    upper_bound = int(now + timedelta(days=2).total_seconds())
    prerecordings = db.zrangebyscore('prerecordings', lower_bound, upper_bound)
    formatted_prerecordings = ''

    index = 0
    for prerecording in prerecordings:
        index += 1
        prerecording = json.loads(prerecording)

        file_path = prerecording['path']

        unicode_time = float(prerecording['time'])
        time = datetime.fromtimestamp(unicode_time)
        time_str = time.strftime('%Hh%Mm')

        end_window = unicode_time + timedelta(minutes=15).total_seconds()
        end_window_str = datetime.fromtimestamp(end_window).strftime('%Hh%Mm')
        # Python represents days with ints 0-6, liquidsoap with ints 1-7
        day_of_week = str(time.weekday() + 1)

        separator = ',' if index < len(prerecordings) else ''
        formatted_prerecordings += ('&nbsp;&nbsp;&nbsp;&nbsp;({' + day_of_week + 'w and ' + time_str + '-'
                                    + end_window_str +  '}, single("' + file_path + '"))' + separator) + '<br>'

    response = 'prerecord = switch([<br>' + formatted_prerecordings + '])'
    return response


if __name__ == '__main__':
    # app.run()
    app.run(debug=True, host='0.0.0.0')
