# README
This repository contains the studio API, the broadcast controls, the prerecordings
upload function and auxiliary files for the recordings page for FreshAir.org.uk,
Edinburgh's student radio station.

## Dependencies
The API is written in Python + Flask. You'll need to install the pip packages
'redis', 'mutagen', 'bleach' and 'requests'.

We use Javascript with JQuery for the front-end. JS dependencies are managed
through Bower (and right now they're checked in in the repository).

## Deployment
To deploy, simply ssh into Megatron and
```
cd /var/www/refresh-broadcastcontrols
git pull
sudo service apache2 restart
```

## Appearance
The Broadcast Controls should mimic the visual styling of refresh-website

## Credits
- Ben Hussey 2012
- Hayden Ball 2013
- Leonardo Mazzone 2017
