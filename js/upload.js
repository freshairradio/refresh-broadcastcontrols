$(document).ready(function() {
    $('#uploadform').ajaxForm({
        url: '/api/prerecording/upload/',
        beforeSubmit: function validator(){ return $("#uploadform").validate(); },
        beforeSend: function(){
            $('#uploadform').slideUp('slow')
            $('#progress').slideDown('slow')
        },
        success: function(){
          let fileName = controller.getFileName();
          let show = controller.getSelectedShow();
          let broadcastTime = controller.getBroadcastTime();


            $.ajax({
                type: "POST",
                url: "/api/prerecording/register/",
                data: { slug: show.slug, broadcastTime: broadcastTime, fileName: fileName, pic: show.pic, title: show.title, link: show.link }
            }).done(function( msg ) {
                $('#success').html("Upload Successful.")
            }).fail(function() {
                $('#uploadform').slideDown('slow')
                $('#progress').slideUp('slow')
                $('#error').html("Sending Meta Failed - Contact webmaster@freshair.org.uk")
            })
        },
        error: function(){
            $('#uploadform').slideDown('slow')
            $('#progress').slideUp('slow')
            $('#error').html("Upload Failed.")
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $('progress').attr({value:position,max:total});
        }
    });
});
