let controller = {

  authenticated: function() {
    model.authenticated();
    ui.authenticated();
  },

  isAuth: function() {
    return model.isAuth;
  },

  fetchShows: function(email) {
    api.fetchShows(email, controller.updateShows);
  },

  updateShows: function(shows) {
    if (shows !== null) {
      model.registerShows(shows);
      ui.updateShows(model.shows);
    } else {
      ui.disableShows();
    }
  },

  getShow: function(slug) {
    return model.shows[slug];
  },

  getShows: function() {
    return model.shows;
  },

  getSelectedShow: function() {
    let fetchedShow = controller.getShow(model.selectedShow);
    if (fetchedShow == undefined) {
      return null;
    }
    return fetchedShow;
  },

  selectShow: function(slug) {
    model.selectShow(slug);
  },

  login: function(slug) {
    if (slug !== 'dummy' && controller.isAuth()) {
      controller.selectShow(slug);
      ui.login(slug);
      api.postBroadcastInfo(controller.getSelectedShow());
    }
  },

  logout: function() {
    model.clearSelectedShow();
    ui.logout();
  },

  checkLive: function() {
    api.checkLive(ui.isLive, ui.notLive);
  },

  goLive: function() {
    api.startLive();
  },

  stopLive: function() {
    api.stopLive();
    controller.logout();
  },

  fetchMessages: function() {
    api.fetchMessages();
  },

  updateMessages: function(messages) {
    ui.updateMessages(messages);
  },

  setBroadcastTime: function(string) {
    model.broadcastTime = moment(string).unix();
  },

  getBroadcastTime: function() {
    return model.broadcastTime;
  },

  setFileName: function(fileName) {
    model.fileName = fileName;
    ui.updateFileName(fileName);
  },

  getFileName: function() {
    return model.fileName;
  },

  uploadStepOne: function() {
    ui.uploadStepOne();
  },

  uploadStepTwo: function(slug) {
    if(slug !== 'dummy') {
      controller.selectShow(slug);
      ui.uploadStepTwo(slug);
    }
  },

  uploadCheckStepTwo: function(slug, datetime) {
    if(datetime !== '') {
      api.checkBroadcastTime(slug, moment(datetime).unix(), controller.uploadStepThree);
    }
  },

  uploadStepThree: function(msg, time) {
    if (msg === 'ok\n') {
      controller.setBroadcastTime(time);
      ui.uploadStepThree();
    }
  },

  getLoginRecords: function(startTime, endTime) {
    api.fetchLoginRecords(startTime, endTime, ui.insertLoginRecords);
  }


}
