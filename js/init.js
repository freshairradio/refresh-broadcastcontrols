$(document).ready(function() {
  const RATE = 5000;


  // Broadcast controls
  api.doAuth();

  controller.checkLive();
  setInterval(function(){ controller.checkLive() }, RATE);

  api.getCurrentSong();
  setInterval(function(){ api.getCurrentSong() }, RATE);

  controller.logout();
  ui.disableShows();

  controller.fetchMessages();
  setInterval(function() { controller.fetchMessages() }, RATE);

  phones.get_status();
  setInterval(function() { phones.get_status() }, RATE);

  $('#phone_status').click(function () {
    alert("Phones cannot be controlled through the web page.\nPlease dial *280");
  });

  $('#song_form input').attr('disabled', 'disabled')
  $('#song_form input').click(function() {
    $this = $(this);
    if ($this.val() == "") {
      $this.val($this.attr('placeholder'));
    }
  });

  $('#email-field').keydown(function(e) {
    controller.fetchShows(e.target.value);
  });

  $('#login-button').click(function() {
    let slug = document.getElementById('show-selector').value;
    controller.login(slug);
  });

  $('#logout-button').click(function() {
    controller.logout();
  });


  // Prerecording upload
  controller.uploadStepOne();

  $('#file').change(function () {
    controller.setFileName(this.value.replace(/^.*\\/, ""));
  });

  $('#filename').click(function () {
    $('#file').click();
  });

  $('#email-field').keyup(function(e) {
    controller.fetchShows(e.target.value);
  });



  // Login records
  let startInput = document.getElementById('start-time');
  let endInput = document.getElementById('end-time');
  let startDate = "0";
  let endDate = "+inf";
  $('#fetch-records-button').click(function() {
    if (startInput.value !== "") startDate = moment(startInput.value).unix();
    if (endInput.value !== "") endDate = moment(endInput.value).unix();

    controller.getLoginRecords(startDate, endDate);
  });
});


String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
