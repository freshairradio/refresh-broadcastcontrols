$(function () {
  var video  = $('#webcam_video')[0];
  var canvas = $('#webcam_canvas')[0];
  var ctx = canvas.getContext('2d');
  var localMediaStream = null;

  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || null;

  navigator.getUserMedia({video: true}, function(stream) {
    video.src = window.URL.createObjectURL(stream);

    canvas.height = video.videoHeight;
    canvas.width  = video.videoWidth;

    localMediaStream = stream;
  }, function(e) {
    console.log('Reeeejected!', e);
  });

  function upload_snapshot() {
    if (localMediaStream) {
      ctx.drawImage(video, 0, 0);

      var image_url = canvas.toDataURL('image/png');
      var blob      = dataURItoBlob(image_url);

      $.ajax({
        url: "http://studio.freshair.org.uk/webcam_upload.php?cam=outside_broadcast",
        method: "PUT",
        data: blob,
        processData: false,
        contentType: false
      });
    }
  }

  setInterval(upload_snapshot, 5 * 1000);
});

function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  return new Blob([ab], { type: 'image/png' });
}