<?php

if( $_SERVER['REQUEST_METHOD'] == 'PUT' ) {
    $cam_name = $_GET['cam'];

    $in  = fopen('php://input', 'r');
    $out = fopen('/export/webcam/upload/'.$cam_name.'.png.tmp', 'w');
        
    while ( $data = fread($in, 4096) ) {
        fwrite($out, $data);
    }

    fclose($out);
    fclose($in);
    
    rename('/export/webcam/upload/'.$cam_name.'.png.tmp', '/export/webcam/upload/'.$cam_name.'.png');
}

